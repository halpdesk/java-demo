package se.halpa.demo.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import se.halpa.demo.models.Employee;

/**
 * EmployeeRepository
 */
public interface EmployeeRepository extends JpaRepository<Employee, Long> {

}
