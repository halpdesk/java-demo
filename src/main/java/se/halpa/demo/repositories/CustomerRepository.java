package se.halpa.demo.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import se.halpa.demo.models.Customer;

/**
 * CustomerRepository
 */
public interface CustomerRepository extends JpaRepository<Customer, Long> {


}
