package se.halpa.demo.bootstrap;

import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import se.halpa.demo.models.Customer;
import se.halpa.demo.repositories.CustomerRepository;

/**
 * DatabaseSeeder
 */
@Component
public class DatabaseSeeder implements CommandLineRunner {

    private final CustomerRepository customerRepository;

    public DatabaseSeeder(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    @Override
    public void run(String... args) throws Exception {

        System.out.println("Loading customer data");

        Customer customer = new Customer();
        customer.setName("Test");
        Customer savedCustomer = this.customerRepository.save(customer);

        System.out.println(savedCustomer.toString());
        System.out.println("Done loading customer data");
    }

}
