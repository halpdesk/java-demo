package se.halpa.demo.models;

import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Data;

/**
 * Customer
 */
@Data
@Entity
@Table( name = "customers" )
public class Customer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String name;

    // @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;

    // @Temporal(TemporalType.TIMESTAMP)
    private Date updatedAt;
}
