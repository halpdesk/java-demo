package se.halpa.demo.models;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Data;

/**
 * Employee
 */
@Data
@Entity
@Table( name = "employees" )
public class Employee {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private long customerId;

    private String firstname;

    private String lastname;

    private String title;

    private Integer salary;

    private enum Gender {
        MALE,
        FEMALE
    }

    // @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;

    // @Temporal(TemporalType.TIMESTAMP)
    private Date updatedAt;
}
