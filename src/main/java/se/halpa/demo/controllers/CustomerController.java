package se.halpa.demo.controllers;

import org.springframework.web.bind.annotation.RestController;

import se.halpa.demo.services.CustomerServiceImpl;
import se.halpa.demo.models.Customer;

import java.util.List;

import javax.websocket.server.PathParam;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;



/**
 * CustomerController
 */
@RestController
@RequestMapping(value="/customers", produces = "application/json")
public class CustomerController {

    private CustomerServiceImpl customerService;

    public CustomerController(CustomerServiceImpl customerService) {
        this.customerService = customerService;
    }

    @GetMapping
    public List<Customer> index() {
        return this.customerService.findAllCustomers();
    }

    @GetMapping("/{id}")
    public Customer show(@PathVariable Long id) {
        return this.customerService.findCustomerById(id);
    }

    @PostMapping
    public Customer store(@RequestBody Customer customer) {
        return this.customerService.saveCustomer(customer);
    }

    @PutMapping("/{id}")
    public Customer update(@RequestBody Customer customer, @PathVariable Long id) {
        return this.customerService.updateCustomer(id, customer);
    }

    @DeleteMapping("/{id}")
    public boolean destroy(@PathVariable Long id) {
        return this.customerService.deleteCustomer(id);
    }




}
