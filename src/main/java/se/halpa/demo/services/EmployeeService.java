package se.halpa.demo.services;

import java.util.List;

import se.halpa.demo.models.Employee;

/**
 * EmployeeService
 */
public interface EmployeeService {

    Employee findEmployeeById(Long id);

    List<Employee> findAllEmployees();

    Employee saveEmployee(Employee employee);
}
