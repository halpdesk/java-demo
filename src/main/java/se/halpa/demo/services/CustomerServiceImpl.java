package se.halpa.demo.services;

import java.util.List;

import org.springframework.stereotype.Service;

import se.halpa.demo.models.Customer;
import se.halpa.demo.repositories.CustomerRepository;

/**
 * CustomerService
 */
@Service
public class CustomerServiceImpl implements CustomerService {

    private final CustomerRepository customerRepository;

    public CustomerServiceImpl(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    @Override
    public Customer findCustomerById(Long id) {
        return this.customerRepository.getOne(id);
    }

    @Override
    public List<Customer> findAllCustomers() {
        return this.customerRepository.findAll();
    }

    @Override
    public Customer saveCustomer(Customer customer) {
        Customer newCustomer = this.customerRepository.save(customer);
        return newCustomer;
    }

    @Override
    public Customer updateCustomer(Long id, Customer customer) {
        Customer existingCustomer = this.customerRepository.getOne(id);
        existingCustomer.setName(customer.getName());
        return existingCustomer;
    }

    @Override
    public boolean deleteCustomer(Long id) {
        Customer customer = this.customerRepository.getOne(id);
        this.customerRepository.delete(customer);
        return true;
    }
}
