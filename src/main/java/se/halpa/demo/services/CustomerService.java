package se.halpa.demo.services;

import java.util.List;

import se.halpa.demo.models.Customer;

/**
 * CustomerService
 */
public interface CustomerService {

    Customer findCustomerById(Long id);

    List<Customer> findAllCustomers();

    Customer saveCustomer(Customer customer);

    Customer updateCustomer(Long id, Customer customer);

    boolean deleteCustomer(Long id);
}
