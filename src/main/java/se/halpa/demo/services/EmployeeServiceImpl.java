package se.halpa.demo.services;

import java.util.List;

import org.springframework.stereotype.Service;

import se.halpa.demo.models.Employee;
import se.halpa.demo.repositories.EmployeeRepository;

/**
 * EmployeeService
 */
@Service
public class EmployeeServiceImpl implements EmployeeService {

    private final EmployeeRepository employeeRepository;

    public EmployeeServiceImpl(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    @Override
    public Employee findEmployeeById(Long id) {
        return this.employeeRepository.getOne(id);
    }

    @Override
    public List<Employee> findAllEmployees() {
        return this.employeeRepository.findAll();
    }

    @Override
    public Employee saveEmployee(Employee employee) {
        Employee newEmployee = this.employeeRepository.save(employee);
        return newEmployee;
    }
}
