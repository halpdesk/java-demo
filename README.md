# Start

## Install Java and Maven

### Debian repositories

- Add unstable sources

```/etc/apt/sources.list
deb http://http.debian.net/debian unstable main contrib non-free
deb-src http://http.debian.net/debian unstable main contrib non-free
```

`# apt install openjdk-12-jdk`

### Maven

Download binaries from maven site: https://maven.apache.org/download.cgi

## Environment variables

```sh
export PATH="$PATH:/opt/apache-maven-3.6.1/bin"
export JAVA_HOME=$(readlink -f /usr/bin/java | sed "s:bin/java::")
```

Alternatively, for JAVA_HOME:

```sh
export JAVA_HOME="/usr/lib/jvm/java-12-openjdk-amd64/"
```

## Install project and run

```sh
mvn install
mvn compile && java -jar target/demo-0.0.1-SNAPSHOT.jar
```
